import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  frase :any= {
    mensaje:'La sabiduría viene de la experiencia. La experiencia. La es a menudo ',
    autor:'Terry'
  };

  mostrar: boolean = true;

  valorNumerico:number = 1;
  personas:string [] = ["Pedro", "Carlos", "maria"];



  constructor() { }

  ngOnInit(): void {
  }

}
